package com.enorkus.academy;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ItAcademyJavaBackendApplication {

	public static void main(String[] args) {
		SpringApplication.run(ItAcademyJavaBackendApplication.class, args);
	}

}

