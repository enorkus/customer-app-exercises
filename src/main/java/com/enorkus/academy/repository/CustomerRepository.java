package com.enorkus.academy.repository;

import com.enorkus.academy.entity.Customer;

import java.util.ArrayList;
import java.util.List;

public class CustomerRepository {

    private static final List<Customer> customers = new ArrayList<>();

    public List<Customer> findAll() {
        return customers;
    }

    public void insert(Customer customer) {
        customer.setId(customer.toString());
        customers.add(customer);
    }

    public void deleteById(String customerId) {
        Customer customerToDelete = null;
        for (Customer customer : customers) {
            if (customer.getId().equals(customerId)) {
                customerToDelete = customer;
            }
        }
        customers.remove(customerToDelete);
    }
}
